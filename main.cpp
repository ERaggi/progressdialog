#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <progressdialog.h>
#include <QQuickView>


int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQuickView view;
    ProgressDialog progressDialog;

    QQmlApplicationEngine engine;
    // One way but it does not work
    // engine.rootContext()->setContextProperty("control", QVariant::fromValue(progressDialog.refModel()));
    // Another way but this also does not work
    view.setSource(QUrl::fromLocalFile("main.qml"));

    const QUrl url(QStringLiteral("qrc:/main.qml"));

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}

#include "progressdialog.h"
#include <QtConcurrent/QtConcurrentMap>

// Spinning function
void spin(int &iteration)
{
    const int work = 1000 * 1000 * 40;
    volatile int v = 0;
    for(int j = 0; j < work; ++j)
        ++v;
}

ProgressDialog::ProgressDialog()
    : m_progressMinimum(0)
    , m_progressMaximum(0)
    , m_progressValue(0)
{
    // Forward state change signals from the watcher to our 'active' property
    connect(&m_futureWatcher, SIGNAL(started()), this, SIGNAL(activeChanged()));
    connect(&m_futureWatcher, SIGNAL(finished()), this, SIGNAL(activeChanged()));
    // Monitor progress chnages of the future to update our own properties
    connect(&m_futureWatcher, SIGNAL(progressRangeChanged(int, int)), this, SLOT(progressRangeChanged(int, int)));
    connect(&m_futureWatcher, SIGNAL(progressValueChanged(int, int)), this, SLOT(progressValueChanged(int, int)));
    connect(&m_futureWatcher, SIGNAL(finished()), this, SLOT(calculationFinished()));
}

int ProgressDialog::numberOfCores() const
{
    //The QtConcurrent framework uses QThreadPool::globalInstance()
    //for computation and that one uses as many thread contexts as
    //returned by QThread::idelThreadCount().
    return QThread::idealThreadCount();
}

bool ProgressDialog::active() const
{
    // Return whether there is currently an active Future
    return m_futureWatcher.isRunning();
}

int ProgressDialog::progressMinimum() const
{
    return m_progressMinimum;
}

int ProgressDialog::progressMaximum() const
{
    return m_progressMaximum;
}

int ProgressDialog::progressValue() const
{
    return m_progressValue;
}

void ProgressDialog::startComputation()
{
//    // Prepare the vector
    QVector<int> vector;
    for(int i = 0; i < 40; ++i)
        vector.append(i);
//    // start the computation.
    const QFuture<void> future = QtConcurrent::map(vector, spin);
//    // Let the future watcher monitor the progress of this Future
    m_futureWatcher.setFuture(future);
}

void ProgressDialog::cancelComputation()
{
    // Stop computation
    m_futureWatcher.cancel();
}

void ProgressDialog::progressRangeChanged(int minimum, int maximum)
{
    m_progressMinimum = minimum;
    m_progressMaximum = maximum;
    emit progressRangeChanged(minimum, maximum);
}

void ProgressDialog::progressValuechanged(int value)
{
    if(m_progressValue == value)
        return;

    m_progressValue = value;
    emit progressValuechanged(value);
}

void ProgressDialog::calculationFinished()
{
    // Rest of the progress indicator after calculation has finished or was cancelled
    m_progressValue = 0;
    emit progressValueChanged();
}



#ifndef PROGRESSDIALOG_H
#define PROGRESSDIALOG_H

#include <QObject>
#include <QFutureWatcher>

class ProgressDialog : public QObject
{
    Q_OBJECT
    // Makes the number of available CPU cores available to the UI
    Q_PROPERTY(int numberOfCores READ numberOfCores WRITE setNumberOfCores NOTIFY numberOfCoresChanged)
    // Makes the current state of the application available to the UI
    Q_PROPERTY(bool active READ active WRITE setActive NOTIFY activeChanged)
    // Make the minimum of the progress indicator available to the UI
    Q_PROPERTY(int progressMinimum READ progressMinimum WRITE setProgressMinimum NOTIFY progressMinimumChanged)
    // Make the maximum of the progress indicator available to the UI
    Q_PROPERTY(int progressMaximum READ progressMaximum WRITE setProgressMaximum NOTIFY progressMaximumChanged)
    // Makes the current progress value available to the UI
    Q_PROPERTY(int progressValue READ progressValue WRITE setProgressValue NOTIFY progressValueChanged)

public:
    ProgressDialog();
    // Accessing to the properties
    int numberOfCores() const;
    bool active() const;
    int progressMinimum() const;
    int progressMaximum() const;
    int progressValue() const;

//    // I will use this as a reference to pass to main.cpp using setContextProperty()
    QObject &refModel()
    {
        return m_Model;
    }

public Q_SLOTS:
//    // This method is invoked when the user clicks on the start button in the GUI
    void startComputation();
//    // This method is invoked when the user clicks on the cancel button in the GUI
    void cancelComputation();

Q_SIGNALS:
    // The change notification signals of the properties
    void numberOfCoresChanged();
    void activeChanged();
    void progressMinimumChanged();
    void progressMaximumChanged();
    void progressValueChanged();

private Q_SLOTS:
//    // This slot is invoked whenever the future reports a change of the progress
    void progressRangeChanged(int minimum, int maximum);
//    // This slot is invoked whenever the future reports a change of the current progress
    void progressValuechanged(int value);
//    // This slot is invoked whenever the future reports to be finished
    void calculationFinished();

private:
    QObject m_Model;
//    // The future watcher that provides monitoring for the currently running computation
    QFutureWatcher<void> m_futureWatcher;
//    // the minimum progress value
    int m_progressMinimum;
//    // the maximum progress value
    int m_progressMaximum;
//    // the current progress value
    int m_progressValue;
};

#endif // PROGRESSDIALOG_H
